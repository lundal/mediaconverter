.PHONY: push image binary

image_name = registry.gitlab.com/lundal/mediaconverter
image_version = $(shell git branch --show-current)

server_binary = server/build/application

# Push docker image to registry
push: image
	docker push $(image_name):$(image_version)

# Build docker image
image: $(server_binary) Dockerfile
	docker build --tag $(image_name):$(image_version) .

# Build binary
binary: $(server_binary)

# Build server binary
$(server_binary): $(shell find server/src -type f) $(shell find server -maxdepth 1 -type f)
	cd server && ./gradlew clean
	cd server && ./gradlew build --info
	mv server/build/*-runner $(server_binary)
