FROM archlinux:latest

# Install dependencies
RUN pacman --sync --refresh --sysupgrade --noconfirm imagemagick libheif libavif libwebp ffmpeg \
 && rm --recursive --force /var/cache

# Install app
WORKDIR /app
COPY server/build/application /app/application

EXPOSE 3202

CMD ["./application"]
