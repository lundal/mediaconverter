package io.lundal.mediaconverter.auth

import io.lundal.mediaconverter.exceptions.ServiceExceptions
import io.vertx.ext.web.RoutingContext
import org.apache.commons.codec.digest.DigestUtils
import org.eclipse.microprofile.config.inject.ConfigProperty
import javax.enterprise.context.RequestScoped

@RequestScoped
class AuthenticationService(
    private val routingContext: RoutingContext,
    @ConfigProperty(name = "allowed-api-key-hashes") private val allowedApiKeyHashes: String,
) {

    fun requireApp() {
        val authorization = routingContext.request().getHeader("authorization") ?: throw ServiceExceptions.missingAuthorizationHeader()
        if (!authorization.startsWith("api-key ")) throw ServiceExceptions.unsupportedAuthenticationScheme()
        if (DigestUtils.sha3_256Hex(authorization.removePrefix("api-key ")) !in allowedApiKeyHashes.split(",")) throw ServiceExceptions.invalidApiKey()
    }

}
