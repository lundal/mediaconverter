package io.lundal.mediaconverter.exceptions

object ServiceExceptions {

    // Auth
    fun missingAuthorizationHeader() = ServiceException(401, "Not authenticated", "Missing authorization header")
    fun unsupportedAuthenticationScheme() = ServiceException(401, "Not authenticated", "Unsupported authenticaton scheme")
    fun invalidApiKey() = ServiceException(401, "Not authenticated", "Invalid api key")

    // General
    fun invalidJson(details: String) = ServiceException(400, "Invalid JSON", details)
    fun validationFailed(violations: String) = ServiceException(400, "Validation failed", violations)
    fun resourceNotFound() = ServiceException(404, "Resource not found")
    fun unexpectedError(e: Exception) = ServiceException(500, "Unexpected error", null, e)

}
