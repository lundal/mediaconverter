package io.lundal.mediaconverter.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
enum class ImageFormat(val extension: String) {
    JPEG("jpeg"),
    PNG("png"),
    WEBP("webp"),
    AVIF("avif")
}
