package io.lundal.mediaconverter.api

import io.lundal.mediaconverter.api.model.ConvertImageRequest
import io.lundal.mediaconverter.api.model.Image
import io.lundal.mediaconverter.auth.AuthenticationService
import io.lundal.mediaconverter.exec.ProcessService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import javax.validation.Valid
import javax.validation.constraints.NotNull
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType.APPLICATION_JSON

@Path("/api/v1/images")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
class ImageApi(
    private val authenticationService: AuthenticationService,
    private val processService: ProcessService,
) {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @POST
    @Path("/convert")
    fun convertImage(@Valid @NotNull request: ConvertImageRequest): List<Image> {
        authenticationService.requireApp()

        val dir = createTempDir("images")
        try {
            logger.info("Input image is ${request.image.format}")
            val source = dir.resolve("source.${request.image.format.extension}")
            source.writeBytes(request.image.data)

            return request.specs.map { spec ->
                logger.info("Converting to $spec")
                val target = dir.resolve("target-${spec.size.width}x${spec.size.height}.${spec.format.extension}")
                convert(source, target, spec.size.width, spec.size.height, spec.quality)
                Image(
                    format = spec.format,
                    data = target.readBytes()
                )
            }
        } finally {
            dir.deleteRecursively()
        }
    }

    private fun convert(source: File, target: File, width: Int, height: Int, quality: Int) {
        processService.run(
            listOf(
                "convert", source.absolutePath,
                "-resize", "${width}x${height}>",
                "-quality", "$quality",
                target.absolutePath
            )
        )
    }

}
