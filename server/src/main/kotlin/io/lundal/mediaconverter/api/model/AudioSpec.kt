package io.lundal.mediaconverter.api.model

import io.quarkus.runtime.annotations.RegisterForReflection
import javax.validation.Valid
import javax.validation.constraints.Positive

@RegisterForReflection
data class AudioSpec(
    @field:Valid
    val format: AudioFormat,

    @field:Positive
    val bitrate: Int,
)
