package io.lundal.mediaconverter.api.model

import io.quarkus.runtime.annotations.RegisterForReflection
import javax.validation.Valid
import javax.validation.constraints.NotEmpty

@RegisterForReflection
data class ConvertAudioRequest(
    @field:Valid
    val audio: Audio,

    @field:Valid
    @field:NotEmpty
    val specs: List<AudioSpec>
)
