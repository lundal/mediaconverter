package io.lundal.mediaconverter.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
enum class AudioFormat(val extension: String) {
    MP3("mp3"),
    WAV("wav"),
    OGG("ogg"),
    OPUS("opus")
}
