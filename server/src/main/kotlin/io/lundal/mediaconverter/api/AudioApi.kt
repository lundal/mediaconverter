package io.lundal.mediaconverter.api

import io.lundal.mediaconverter.api.model.Audio
import io.lundal.mediaconverter.api.model.ConvertAudioRequest
import io.lundal.mediaconverter.auth.AuthenticationService
import io.lundal.mediaconverter.exec.ProcessService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import javax.validation.Valid
import javax.validation.constraints.NotNull
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType.APPLICATION_JSON

@Path("/api/v1/audio")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
class AudioApi(
    private val authenticationService: AuthenticationService,
    private val processService: ProcessService,
) {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @POST
    @Path("/convert")
    fun convertAudio(@Valid @NotNull request: ConvertAudioRequest): List<Audio> {
        authenticationService.requireApp()

        val dir = createTempDir("audio")
        try {
            logger.info("Input audio is ${request.audio.format}")
            val source = dir.resolve("source.${request.audio.format.extension}")
            source.writeBytes(request.audio.data)

            return request.specs.map { spec ->
                logger.info("Converting to $spec")
                val target = dir.resolve("target-${spec.bitrate}.${spec.format.extension}")
                convert(source, target, spec.bitrate)
                Audio(
                    format = spec.format,
                    data = target.readBytes()
                )
            }
        } finally {
            dir.deleteRecursively()
        }
    }

    private fun convert(source: File, target: File, bitrate: Int) {
        processService.run(
            listOf(
                "ffmpeg",
                "-i", source.absolutePath,
                "-b:a", "$bitrate",
                target.absolutePath
            )
        )
    }

}
