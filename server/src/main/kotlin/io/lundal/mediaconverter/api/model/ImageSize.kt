package io.lundal.mediaconverter.api.model

import io.quarkus.runtime.annotations.RegisterForReflection
import javax.validation.constraints.Positive

@RegisterForReflection
data class ImageSize(
    @field:Positive
    val width: Int,

    @field:Positive
    val height: Int,
)
