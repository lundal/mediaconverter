package io.lundal.mediaconverter.exec

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class ProcessService {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    fun run(command: List<String>) {
        logger.debug("Running $command")

        val before = System.currentTimeMillis()

        val process = ProcessBuilder()
            .command(command)
            .redirectOutput(if (logger.isDebugEnabled) ProcessBuilder.Redirect.INHERIT else ProcessBuilder.Redirect.DISCARD)
            .redirectError(if (logger.isDebugEnabled) ProcessBuilder.Redirect.INHERIT else ProcessBuilder.Redirect.DISCARD)
            .start()

        if (!process.waitFor(30, TimeUnit.SECONDS)) {
            logger.info("${command.first()} timed out")
            process.destroy()
        }

        if (process.exitValue() != 0) {
            logger.info("${command.first()} exited with status ${process.exitValue()}")
            throw RuntimeException("Exit status ${process.exitValue()}")
        }

        val after = System.currentTimeMillis()
        logger.info("${command.first()} completed after ${after - before} ms")
    }

}
